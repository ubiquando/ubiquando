#!/bin/bash
if [ -z $1 ];
then
    echo "Please add date in YYYY-MM-DD format as first parameter of this helper script"
    exit 1
fi 
log=../data/${1}/log/log_step_2.txt
source py3env/bin/activate
echo "Starting $0 at $(date)" >>$log

echo "Start ListMetadataFormats Workers" >>$log
{ time ./OAIPMH_worker.py \
    --verb "ListMetadataFormats" \
    --date $1 \
    --workers 2 \
    --number 0 & } >>${log}0 2>&1
pids[0]=$!
{ time ./OAIPMH_worker.py \
    --verb "ListMetadataFormats" \
    --date $1 \
    --workers 2 \
    --number 1 & } >>${log}1 2>&1
pids[1]=$!

echo "Start Identify Workers" >>$log 
{ time ./OAIPMH_worker.py \
    --verb "Identify" \
    --date $1 \
    --workers 2 \
    --number 0 & } >>${log}2 2>&1
pids[2]=$!
{ time ./OAIPMH_worker.py \
    --verb "Identify" \
    --date $1 \
    --workers 2 \
    --number 1 & } >>${log}3 2>&1
pids[3]=$!

# Wait for workers to finish!
echo "Wait for workers to finish" >>$log
i=0
for pid in ${pids[*]}
do
    wait $pid;
    echo "===START WORKER $i ===" >>$log
    cat ${log}$i >> $log
    echo "===END WORKER $i ===" >>$log
    rm ${log}$i
    i=$((i+1)) 
done

echo "Start Processing MetadataFormats and Identify-Output (Combining Results) at $(date)" >>$log 2>&1
{ time ./step2_combineWorker.py --date $1; } >>$log 2>&1

echo "Finishing $0 at $(date)" >>$log
