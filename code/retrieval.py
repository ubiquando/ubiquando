#!/usr/bin/env python3 
import re
import pprint
import urllib.parse
import urllib.request

def pangaeaRetrieve(url, type):
    with urllib.request.urlopen(url) as response:
        headers = response.info() 
        for link in headers["Link"].split(","):
            linkParts = link.split(";")
            if linkParts[1] == 'rel="item"':
                print(linkParts[0].strip()[1:-1])

    
#with open("test") as f:
#    s = f.read()
#    linkLine = re.findall(r'Link:.*', s)[0]
#    linkLine = re.sub('Link:', '', linkLine).strip().split(",")
#    for link in linkLine:
#        linkParts = link.split(";")
#        if linkParts[1] == 'rel="item"':
#            print(linkParts[0].strip()[1:-1])

print(pangaeaRetrieve("https://doi.pangaea.de/10.1594/PANGAEA.56773"))
