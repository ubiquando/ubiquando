#!/usr/bin/env python3
import datetime
import os
import pprint
import sys
import urllib.request
import xml.etree.ElementTree as ET

rel_base_dir    = "../data/" + datetime.datetime.now().strftime('%Y-%m-%d')
base_dir        = os.path.abspath(os.path.expanduser(rel_base_dir))
raw_dir         = os.path.join(base_dir, "raw")
retrieve_dir    = os.path.join(raw_dir, "rdrs")
re3dataBaseUrl  = "https://www.re3data.org/api/v1/"

################################################################################
# CREATE FILESTRUCTURE
################################################################################
createDirs = [ base_dir, raw_dir, retrieve_dir ]

for directoryToBeCreated in createDirs:
    try:
        os.stat(directoryToBeCreated)
    except:
        os.mkdir(directoryToBeCreated)


################################################################################
# RETRIEVE RDR LIST
################################################################################
repositoriesFile = os.path.join(retrieve_dir, "repositories.xml")
repositoriesUrl  = re3dataBaseUrl + 'repositories'

if not os.path.isfile(repositoriesFile):
    print("Retrieving %s" % (repositoriesUrl))
    urllib.request.urlretrieve(repositoriesUrl, repositoriesFile)
else:
    print("Reusing existing repositories file %s" % (repositoriesFile))

################################################################################
# ITERATE OVER RDR and store the files
################################################################################
e = ET.ElementTree(ET.parse(repositoriesFile))
rdrIds = e.findall('.//id')
for rdrIdElem in rdrIds:
    rdrId = rdrIdElem.text
    rdr_dir = os.path.join(retrieve_dir, rdrId)
    try:
        os.stat(rdr_dir)
    except:
        os.mkdir(rdr_dir)
    rdrFile = os.path.join(rdr_dir, rdrId)
    if not os.path.isfile(rdrFile):
        rdrRetrieveUrl = re3dataBaseUrl + 'repository/' + rdrId
        print("Retrieving rdr info for %s from %s" % (rdrId, rdrRetrieveUrl))
        urllib.request.urlretrieve(rdrRetrieveUrl, rdrFile)
    else:
        print("Reusing existing rdr file %s" % (rdrFile))
