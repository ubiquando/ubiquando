#!/usr/bin/env python3
import csv
import rdr 
import sys
import os
import pprint
import argparse
import helper

################################################################################
# CONFIGURATION AND PREPARATION
################################################################################

########################################
# CONFIGURATION AND PREPARATION
########################################
# TODO add criteria, timeout and oaiResponseValidator as argument
parser = argparse.ArgumentParser(
    description='Test Distribution of Harvest'
)
parser.add_argument('--date',
        required    = True,
        help        ="Date when re3data.org data were harvested (Format YYYY-MM-DD)")
parser.add_argument('--workersI',
        required    = True,
        help        ="Number of Workers")
parser.add_argument('--workersR',
        required    = True,
        help        ="Number of Workers")
args = parser.parse_args()

date                = args.date
workersI             = int(args.workersI)
workersR             = int(args.workersR)

########################################
# Derived values (do not change)
########################################
rel_base_dir    = "../data/" + date
base_dir        = os.path.abspath(os.path.expanduser(rel_base_dir))
processed_dir   = os.path.join(base_dir, "processed") 
raw_dir         = os.path.join(base_dir, "raw")
rdrs_dir        = os.path.join(raw_dir, "rdrs")

input_rdr_csv   = os.path.join(processed_dir, "step2_rdr.csv")
distribution    = {}
workersmax = max(workersI, workersR)
for checkfor in ["ListIdentifiers", "Datacite"]:
    distribution[checkfor] = {}
    cur_worker = 0
    while cur_worker != workersmax:
        distribution[checkfor][cur_worker] = {} 
        distribution[checkfor][cur_worker]["count"] = 0 
        distribution[checkfor][cur_worker]["RDRs"] = []
        cur_worker += 1

################################################################################
# Check Distribution
################################################################################
with open(input_rdr_csv, 'r') as input_file:
    reader = csv.DictReader(input_file)
    for row in reader:
        if row["OAIPMH_URL_canonical"] == "True":
            rdr_id = row["id"]
            cur_rdr_dir = os.path.join(rdrs_dir, rdr_id) 
            cur_rdr = rdr.RDR(file = os.path.join(cur_rdr_dir, rdr_id))
            # ListIdentifiers Distribution            
            worker_number = helper.getHashForRDR(rdr_id, workersI)
            distribution["ListIdentifiers"][worker_number]["count"] += 1
            distribution["ListIdentifiers"][worker_number]["RDRs"].append([rdr_id, cur_rdr.getName()])


            worker_number = helper.getHashForRDR(rdr_id, workersR)
            if row["MDF_Datacite"]:
            # ListRecords QualifiedDC Distribution
                distribution["Datacite"][worker_number]["count"] += 1 
                distribution["Datacite"][worker_number]["RDRs"].append([rdr_id, cur_rdr.getName()])
pprint.pprint(distribution)
