#!/usr/bin/env python3
from lxml import etree
from lxml import objectify
import csv
import json
import datetime
import os
import socket
import sys
import urllib.request
import urllib
import argparse
import pprint
import re
import dateutil.parser
import textwrap
import datetime
from helper import getHashForRDR, getReqUrl

################################################################################
# CONFIGURATION AND PREPARATION
################################################################################
parser = argparse.ArgumentParser(
    description='Step 3: Retrieve metadata from RDRs'
)
parser.add_argument('--date',
        required    = True,
        help        ="Date when re3data.org data were harvested (Format YYYY-MM-DD)")
parser.add_argument('--workers',
        default     = 1,
        help        ="Number of workers spawned")
parser.add_argument('--number',
        default     = 0,
        help        ="Number of this worker, starting at 0")
parser.add_argument('--mdf',
        default     = "DublinCore",
        help        ="Canonical name of metadata format", 
        choices     = set(("DublinCore", "Datacite")))
parser.add_argument('--verb',
        required    = True,
        help        ="OAI-PMH verb",
        choices     = set(("ListIdentifiers", "ListRecords", "Identify", "ListMetadataFormats")))

args = parser.parse_args()

oaiResponseValidator = "util/oaiResponse.xsd"
date                = args.date
verb                = args.verb
mdf                 = args.mdf
workers             = int(args.workers)
number              = int(args.number)
timeout             = 20

print("Worker %i: Starting processing at %s\n\tverb: %s\n\tmdf: %s\n\tworkers: %i\n\tdate: %s" % (
    number,
    datetime.datetime.now().isoformat(),
    verb,
    mdf,
    workers,
    date))

########################################
# Derived values (do not change)
########################################
rel_base_dir    = "../data/" + date
base_dir        = os.path.abspath(os.path.expanduser(rel_base_dir))
processed_dir   = os.path.join(base_dir, "processed")
raw_dir         = os.path.join(base_dir, "raw")
rdrs_dir        = os.path.join(raw_dir, "rdrs")

if verb == "ListRecords" or verb == "ListIdentifiers":
    input_rdr_csv   = os.path.join(processed_dir, "step2_rdr.csv")
    output_rdr_json = os.path.join(processed_dir, 
            "step3_rdr_" + verb + "_" + mdf + "_" + str(number) + ".json")
elif verb == "Identify" or verb == "ListMetadataFormats": 
    input_rdr_csv   = os.path.join(processed_dir, "step1_rdr.csv")
    output_rdr_json = os.path.join(processed_dir,
            "step2_rdr_" + verb + "_" + str(number) + ".json") 

data            = {}

# Make sure that the retrieve directory exists
createDirs = [rdrs_dir]
for directoryToBeCreated in createDirs:
    try:
        os.stat(directoryToBeCreated)
    except:
        os.mkdir(directoryToBeCreated)

# Reuse partial results from previous run
try:
    os.stat(output_rdr_json)
    tmp_file_exists = True
    with open(output_rdr_json) as tf:
        data = json.load(tf)
    print("Worker %i: Reusing %s" % (number, output_rdr_json))
except:
    tmp_file_exists = False

with open(oaiResponseValidator, 'r') as xsd_file:
    xmlschema_doc = etree.parse(xsd_file)
    xmlschema = etree.XMLSchema(xmlschema_doc)

class ValidationError(Exception):
    pass

def retrieveChunk(rdr, verb, metadataPrefix, rdr_dir, timeout, resumptionToken = ""):
    """Retrieves and saves a chunk of a OAI-PMH List-verb (ListRecords, ListIdentifiers)
    rdr             -- contains the information for the RDR
    verb            -- either "ListRecords" or "ListIdentifiers" 
    metadataPrefix  -- a value defined by the OAI-PMH ListMetadataPrefix response
    rdr_dir         -- the directory in which the chunks will be saved
    timeout         -- time to wait until OAI-PMH server is assumed unresponsive
    resumptionToken -- append the resumptionToken to retrieve the next chunk
    """
    # GET THE CHUNK
    reqUrl  = getReqUrl(rdr, resumptionToken, verb, metadataPrefix)
    print("Worker %i: Requesting chunk %i: %s" % (number, rdr["OAIPMH_" + verb + "_chunks"], reqUrl))
    try:
        response    = urllib.request.urlopen(reqUrl, None, timeout)
    except (urllib.error.HTTPError, urllib.error.URLError) as err:
        # RETRY with unquoted resumptionToken if we quoted it
        if resumptionToken != "":
            reqUrl = getReqUrl(rdr, resumptionToken, verb, metadataPrefix, False)
            print("Worker %i: Retrying with unquotedResumptionToken!" % number)
            print("Worker %i: Requesting chunk %i: %s" % (number, row["OAIPMH_" + verb + "_chunks"], reqUrl))
            response    = urllib.request.urlopen(reqUrl, None, timeout)
        else:
            raise 
    # Some RDRs need to be awakened
    except (socket.timeout):
        response    = urllib.request.urlopen(reqUrl, None, timeout)
    oairesponse = objectify.parse(response)
    # TODO We have to take care of validation with ListRecords
    # The validation needs to retrieve the scheme for the metadata scheme!
    if verb != "ListRecords" and verb != "Identify":
        if not xmlschema.validate(oairesponse):
            log = xmlschema.error_log
            raise ValidationError(log)

    # SAVE THE CHUNK
    root = oairesponse.getroot()
    # Examples:
    # ListRecords_Datacite_0.xml
    # ListIdentifiers_DublinCore_123.xml
    if verb == "ListRecords" or verb == "ListIdentifiers":
        chunk_file_name = os.path.join(rdr_dir,
            "%s_%s_%i.xml" % (verb, mdf, rdr["OAIPMH_" + verb + "_chunks"]))
    else:
        chunk_file_name = os.path.join(rdr_dir,
            "%s_%i.xml" % (verb, rdr["OAIPMH_" + verb + "_chunks"]))
    objectify.deannotate(root)
    etree.cleanup_namespaces(root)
    chunk_as_string = etree.tostring(root, xml_declaration=True)
    try:
        try:
            ns = root.nsmap[None]
        except KeyError:
            if "oai" in root.nsmap.keys():
                ns = root.nsmap["oai"]
            else:
                raise
        verbElement = root.findall("{" + ns + "}" + verb)[0]
    except:
        message = "Worker %i: Response has no %s element" % (
            number,
            verb)
        print(message)
        raise ValidationError(message)

    # Only store if this is a valid response!
    with open (chunk_file_name, 'wb') as chunk_file:
        chunk_file.write(chunk_as_string)

    # Save ExpirationDate for reusing the results already harvested 
    rdr["OAIPMH_" + verb + "_chunks"] += 1

    try:
        rdr["OAIPMH_" + verb + "_resumptionToken_expirationDate"] = (
                verbElement.resumptionToken.get("expirationDate"))
    except AttributeError:
        pass 

    # Retrieve completeListSize (if given
    try:
        if rdr["OAIPMH_" + verb + "_completeListSize"] == 0:
            completeListSize = verbElement.resumptionToken.get("completeListSize")
            if completeListSize:
                rdr["OAIPMH_" + verb + "_completeListSize"] = (
                    verbElement.resumptionToken.get("completeListSize"))
                print("Worker %i: Answer has attribute completeListSize: %s" %
                    (number, rdr["OAIPMH_" + verb + "_completeListSize"]))
    except AttributeError: 
        pass

    try:
        return str(verbElement.resumptionToken)

    # RETURN RESUMPTIONTOKEN
    # Return "" when no resumptionToken is part of the answer.
    # This means we are at the last chunk
    except AttributeError:
        print("Worker %i: No resumptionToken given with response" % (number))
        return ""

################################################################################
# ITERATE OVER FUNCTIONAL OAI-PMH RDRS AND RETRIEVE IDENTIFIERS 
################################################################################
with open(input_rdr_csv, 'r') as input_file:
    reader = csv.DictReader(input_file)
    for row in reader:
        # We read from the temp file
        if tmp_file_exists:
            try:
                if data[row["id"]]["OAIPMH_" + verb]:
                    print("Worker %i: %s already harvested" % (number, row["id"]))
                    continue
            except KeyError:
                pass
           
        # See whether this worker is responsible
        if not getHashForRDR(row["id"], workers) == number:
            #print("Worker %i not responsible for %s" % (number, row["id"]))
            continue

        # Only set default values, if we haven't already processed this RDR
        # Otherwise we would overwrite the cached result for failed harvesters
        # ("OAIPMH_" + verb is False, but e.g. the resumptionToken might exist) 
        if not row["id"] in data.keys():
            data[row["id"]] = row
            print("Worker %i has no previous run, setting default values" % number)
            data[row["id"]]["OAIPMH_" + verb + "_URL"]                              = ""
            data[row["id"]]["OAIPMH_" + verb ]                                      = False
            data[row["id"]]["OAIPMH_" + verb + "_chunks"]                           = 0
            data[row["id"]]["OAIPMH_" + verb + "_no_reason"]                        = ""
            data[row["id"]]["OAIPMH_" + verb + "_no_details"]                       = ""
            data[row["id"]]["OAIPMH_" + verb + "_completeListSize"]                 = 0
            data[row["id"]]["OAIPMH_" + verb + "_resumptionToken"]                  = "" 
            data[row["id"]]["OAIPMH_" + verb + "_resumptionToken_expirationDate"]   = None
        else:
            row = data[row["id"]]
            print("Worker %i: Continuing for %s" % (number, row["id"]))

        if row["OAIPMH_URL_canonical"] != "True":
            print("Worker %i: %s does not support OAIPMH or is not the canonical RDR for this API" %
                    (number, row["id"]))
        else:

            # If the repository does not support the mdf requested and we have ListRecords as 
            # a verb, skip
            if verb == "ListRecords" and data[row["id"]]["MDF_" + mdf] == "":
                print("Worker %i: Will not harvest %s for %s - not supported by RDR" % (
                    number, mdf, row["id"]))
                continue
            if data[row["id"]]["OAIPMH_" + verb ] == "True":
                print("Worker %i: Already harvested %s, skipping!" % (number, row["id"]))
                continue
            rdr_dir = os.path.join(rdrs_dir, row["id"])
            # Reuse token only when expiration date has not already been elapsed. 
            if row["OAIPMH_" + verb + "_resumptionToken_expirationDate"]:
                expirationDate = dateutil.parser.parse(
                    row["OAIPMH_" + verb + "_resumptionToken_expirationDate"])
                if expirationDate < datetime.datetime.now(datetime.timezone.utc):
                    data[row["id"]]["OAIPMH_" + verb + "_resumptionToken"] = "" 
                else:
                    print("Worker %i cannot use resumption token expirationDate elapsed: %s" %
                            (number, expirationDate.isoformat()))
                    data[row["id"]]["OAIPMH_" + verb + "_resumptionToken"] = ""
                    data[row["id"]]["OAIPMH_" + verb + "_resumptionToken_expirationDate"]   = None
                    data[row["id"]]["OAIPMH_" + verb + "_chunks"]                           = 0
                    data[row["id"]]["OAIPMH_" + verb + "_no_reason"]                        = ""
                    data[row["id"]]["OAIPMH_" + verb + "_no_details"]                       = ""
            try:
                if verb == "ListRecords" or verb == "ListIdentifiers":
                    metadataPrefix = data[row["id"]]["MDF_" + mdf]
                else:
                    metadataPrefix = "dummyValue"
                print("Worker %i: Start retrieving %s (%s - %s) for %s" % (
                    number,
                    verb,
                    mdf,
                    metadataPrefix,
                    row["id"]))
                if data[row["id"]]["OAIPMH_" + verb + "_resumptionToken"] != "":
                    print("Worker %i: Continuing with %s" % (
                        number,
                        data[row["id"]]["OAIPMH_" + verb + "_resumptionToken"]))
                while True:
                    data[row["id"]]["OAIPMH_" + verb + "_resumptionToken"] = (
                            retrieveChunk(
                                data[row["id"]],
                                verb,
                                metadataPrefix,
                                rdr_dir,
                                timeout,
                                data[row["id"]]["OAIPMH_" + verb + "_resumptionToken"])
                    )
                    # since harvesting is very time consuming we will cache
                    # after every chunk
                    with open(output_rdr_json, 'w') as tf:
                        print("Worker %i saved intermediate result in %s" % (number, output_rdr_json))
                        json.dump(data, tf)
                    # no chunks left
                    if data[row["id"]]["OAIPMH_" + verb + "_resumptionToken"] == "":
                        print("Worker %i: Finished retrieving %s (%s - %s) for %s (%i chunks)" % (
                            number,
                            verb,
                            mdf,
                            metadataPrefix,
                            row["id"],
                            row["OAIPMH_" + verb + "_chunks"]
                            ))
                        break
                    # ListIdentifiers is only called to get size of RDRs items
                    if data[row["id"]]["OAIPMH_" + verb + "_completeListSize"] != 0 and verb == "ListIdentifiers":
                        print("Worker %i: %s supports completeListSize - fast track!" % (number, row["id"]))
                        break
                    if data[row["id"]]["OAIPMH_" + verb + "_resumptionToken"] == "":
                        print("Worker %i: Finished retrieving %s (%s) for %s (%i chunks)" % (
                            number,
                            verb,
                            metadataPrefix,
                            row["id"],
                            row["OAIPMH_" + verb + "_chunks"]
                            ))
                        break

            # There is a http error
            except (urllib.error.HTTPError, urllib.error.URLError, socket.timeout, ConnectionResetError) as err:
                print("Worker %i: Problem: %s with the connection to %s" % (number, verb, row["id"]))
                data[row["id"]]["OAIPMH_" + verb + "_no_reason"] = "URL"
                data[row["id"]]["OAIPMH_" + verb + "_no_details"] =  textwrap.shorten(format(err), width=200)
                continue
            # There is an error with the http payload (no valid xml)
            except etree.XMLSyntaxError as err: 
                print("Worker %i: Problem: %s with XML for %s" % (number, verb, row["id"]))
                data[row["id"]]["OAIPMH_" + verb + "_no_reason"] = "XML"
                data[row["id"]]["OAIPMH_" + verb + "_no_details"] =  textwrap.shorten(format(err), width=200)
                continue
            # The xml payload does not validate against the OAI-PMH xsd
            except ValidationError as err:
                print("Worker %i: Problem: %s with XSD for %s" % (number, verb, row["id"])) 
                data[row["id"]]["OAIPMH_" + verb + "_no_reason"] = "VALIDATE"
                data[row["id"]]["OAIPMH_" + verb + "_no_details"] = textwrap.shorten(format(err), width=200)

                continue
            except AttributeError as err:
                print("Worker %i: Problem: %s with the OAI-PMH Payload for %s: %s" % (number, verb, row["id"], str(err))) 
                data[row["id"]]["OAIPMH_" + verb + "_no_reason"] = "No" + verb
                data[row["id"]]["OAIPMH_" + verb + "_no_details"] = textwrap.shorten(format(err), width=200)
                continue
            data[row["id"]]["OAIPMH_" + verb] = True
with open(output_rdr_json, 'w') as tf:
    json.dump(data, tf)

print("Worker %i: Finished processing at %s\n\tverb: %s\n\tmdf: %s\n\tworkers: %i\n\tdate: %s" % (
    number,
    datetime.datetime.now().isoformat(),
    verb,
    mdf,
    workers,
    date))
