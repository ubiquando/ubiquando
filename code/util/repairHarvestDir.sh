#!/bin/bash
rdr_id=$1
date=$2
mdf=$3

dir=/mnt/nfs/ubiquando/data/$date/raw/rdrs/$rdr_id
echo "Repairing $dir"
echo "  (rdr_id: $rdr_id, date: $date, mdf: $mdf)"
for file in `find $dir -regextype sed -regex ".*/[0-9]\+.xml"`
do
    chunk=$(basename $file | awk -F '.xml' '{print $1}')
    dir=$(dirname $file)
    newfilename="$dir/${mdf}_${chunk}.xml"
    echo "$file =>"
    echo "$newfilename"
    mv $file $newfilename
done
