date=$1
ps -elf | egrep 'py|step'
echo "Space"
df -kh | grep nfs
df -k | grep nfs
echo "Inodes"
df -ih | grep nfs
df -i | grep nfs

for file in  ../data/$date/log/log_step_*.txt*
do
    echo "$file: "
    echo -n "   "
    tail  -1 $file 
    echo -n "   "
    ls -lah $file

done
