#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import os.path
import sys
import glob
import hashlib
import pprint
import re
import urllib

def getHashForRDR(RdrId, workers):
    """Returns a number between 0 and workers - 1 to distribute the RDRs amongst several workers
    RdrId   -- Id of the RDR
    workers -- Number of workers the workload is distributed
    """
    figshare_id = "r3d100010066"
    if RdrId == figshare_id:
        return 0
    if workers == 1:
        return 0
    remaining_workers = workers - 1
    hashV = hashlib.sha1(RdrId.encode('utf-8'))
    return ((int(hashV.hexdigest(),16)) % remaining_workers) + 1

def getHashForImage(imageKey, workers):
    """Returns a number between 0 and workers - 1 to distribute Images amongst several workers
    imageKey -- Key of the image 
    workers  -- Number of workers the workload is distributed
    """
    if workers < 2:
        raise AttributeError("At least two workers!") 
    hashV = hashlib.sha1(imageKey.encode('utf-8'))
    return ((int(hashV.hexdigest(),16)) % workers)

def getNumberOfFiles(dir):
    """Returns the number of files in a directory
    dir -- Directory path
    """
    return len([name for name in os.listdir(dir) if os.path.isfile(os.path.join(dir, name))])

def getNumberOfChunks(dir, verb, mdf):
    """Returns the number of chunks in a directory
    dir     -- Director path
    verb    -- OAI-PMH verb of whose reply the chunks are part of
    mdf     -- Metadata format
    """
    return len(glob.glob(dir + "/" + verb + "_" + mdf + "_*.xml"))

def getReqUrl(rdr, resumptionToken, verb, metadataPrefix, quote = True): 
    """Returns the OAI-PMH compliant URL for a List-Verb
    rdr             -- contains the information for the RDR
    resumptionToken -- append the resumptionToken to retrieve the next chunk
    verb            -- "ListRecords|ListIdentifiers|ListMetadataFormats|Identify" 
    quote           -- Whether or not resumptionToken should be quoted (default True)
    """
    # Set base URL
    rdr["OAIPMH_" + verb + "_URL"] = (
        rdr["API_OAI-PMH_URL"] + "?verb=" + verb)
    reqUrl = rdr["OAIPMH_" + verb + "_URL"]
    if resumptionToken != "":
        resumptionToken = str(resumptionToken)
        # simple error correction: if resumption token is already encoded 
        # (some RDS do this), we use it without quoting.
        # see https://www.openarchives.org/OAI/openarchivesprotocol.html#SpecialCharacters
        if re.search(r'%2F|%3F|%23|%3D|%26|%3A|%3B|%20|%25|%2B',
                resumptionToken) or not quote:
            reqResumptionToken = resumptionToken
        else:
            reqResumptionToken = urllib.parse.quote_plus(resumptionToken)
        # OAI-PMH states that resumptionToken is an exclusive parameter,
        # we therefore need to reassemble the URL (dropping metdataPrefix).
        reqUrl = (rdr["OAIPMH_" + verb + "_URL"]
                + "&resumptionToken="
                + reqResumptionToken)
        return reqUrl

    if verb == "ListIdentifiers" and metadataPrefix == "":
        metadataPrefix == "oai_dc"
    elif metadataPrefix != "" and (verb == "ListRecords" or verb == "ListIdentifiers"): 
        reqUrl = ( rdr["OAIPMH_" + verb + "_URL"] 
                + "&metadataPrefix="
                + metadataPrefix)
    return reqUrl 

def urlValid(url):
    try:
        result = urllib.parse.urlparse(url)
        return result.scheme and result.netloc
    except:
        return False
# Inspired by 
# https://stackoverflow.com/questions/120951/how-can-i-normalize-a-url-in-python

def urlFix(s):
    """Sometimes you get an URL by a user that just isn't a real
    URL because it contains unsafe characters like ' ' and so on.  This
    function can fix some of the problems in a similar way browsers
    handle data entered by the user:

    >>> url_fix(u'http://de.wikipedia.org/wiki/Elf (Begriffsklärung)')""" 

    parsed = urllib.parse.urlparse(s)
    path = urllib.parse.quote(parsed.path, '/%')
    query = urllib.parse.quote_plus(parsed.query, ':&=')
    return urllib.parse.urlunparse((
        parsed.scheme,
        parsed.netloc,
        path, 
        parsed.params,
        query,
        parsed.fragment))

def suitableMD(item): 
        #pprint.pprint(item)
        necessarySimpleValues   = ["id", "image", "date"]
        necessaryLists          = ["licenseURI", "geolocation"] 
        for nsv in necessarySimpleValues: 
            if not item[nsv]:
                print("%s: %s" % (nsv, item[nsv]))
                return False 
        pprint(item)
        sys.exit(1)
        for nl in necessaryLists:
            if len(item[nl]) < 1:
                return False
        # Either DateCreated or DateCollected must be set
        if not item["dateCreated"] and not item["dateCollected"]:
            return False
        if not item["image"]:
            return False
        return True

def isValidDOI(s):
    if re.search(r'\b(10[.][0-9]{4,}(?:[.][0-9]+)*/(?:(?!["&\'<>])\S)+)\b', s):
        return True
    else:
        return False
