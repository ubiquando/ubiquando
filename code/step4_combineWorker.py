#!/usr/bin/env python3 
from lxml import etree
from lxml import objectify
import csv
import json 
import os 
import sys
import argparse
import pprint
import glob
import re
import rdr
from helper import getHashForRDR, getNumberOfChunks, getNumberOfFiles

################################################################################
# CONFIGURATION AND PREPARATION
################################################################################
parser = argparse.ArgumentParser(
    description='Step 4: Combine the data collected during harvesting'
)
parser.add_argument('--date',
        required    = True,
        help        ="Date when re3data.org data were harvested (Format YYYY-MM-DD)")
parser.add_argument('--images',
        default     = "",
        help        ="Json-formatted file as images input")
args = parser.parse_args() 

date                = args.date

########################################
# Derived values (do not change)
########################################
rel_base_dir    = "../data/" + date
base_dir        = os.path.abspath(os.path.expanduser(rel_base_dir))
processed_dir   = os.path.join(base_dir, "processed")

if os.path.isfile(os.path.join(processed_dir, args.images)):
        output_images = os.path.join(processed_dir, args.images)
else:
    output_images   = os.path.join(processed_dir, "step4_images.json")

worker_output   = {}
images          = {}

def loadWorkerData(): 
    for worker_file in glob.glob(processed_dir + "/step4_images.json*"):
        regex = r".*\/step4_images.json(\d+)$"
        if not re.match(regex, worker_file):
            continue
        m = re.split(regex, worker_file)
        worker = int(m[1])
        with open(worker_file, 'r') as wf:
            worker_output[worker] = json.load(wf)

################################################################################
# ITERATE OVER ALL STEP 4 JSON FILES AND  
################################################################################

loadWorkerData()
number_of_workers = max(worker_output, key=int) + 1

# Combine images files
for worker in worker_output:
    images.update(worker_output[worker])

################################################################################
# SAVE OUTPUT 
################################################################################
with open(output_images, 'w') as itf:
    json.dump(images, itf)
