#!/usr/bin/env python3 
from lxml import etree
from lxml import objectify
import csv
import json 
import os 
import sys
import argparse
import pprint
import glob
import re
import rdr
from helper import getHashForRDR, getNumberOfChunks, getNumberOfFiles

################################################################################
# CONFIGURATION AND PREPARATION
################################################################################
parser = argparse.ArgumentParser(
    description='Step 3: Combine the data collected during harvesting'
)
parser.add_argument('--date',
        required    = True,
        help        ="Date when re3data.org data were harvested (Format YYYY-MM-DD)")
args = parser.parse_args() 

date                = args.date

########################################
# Derived values (do not change)
########################################
rel_base_dir    = "../data/" + date
base_dir        = os.path.abspath(os.path.expanduser(rel_base_dir))
processed_dir   = os.path.join(base_dir, "processed")
raw_dir         = os.path.join(base_dir, "raw")
rdrs_dir        = os.path.join(raw_dir, "rdrs")

input_rdr_csv   = os.path.join(processed_dir, "step2_rdr.csv")
output_rdr_csv  = os.path.join(processed_dir, "step3_rdr.csv")
worker_output   = {}
data            = {}

retrieveFromWorkers = [
        "OAIPMH_ListRecords_URL",
        "OAIPMH_ListRecords",
        "OAIPMH_ListRecords_chunks",
        "OAIPMH_ListRecords_no_reason",
        "OAIPMH_ListRecords_no_details",
        "OAIPMH_ListRecords_completeListSize",
        "OAIPMH_ListRecords_resumptionToken",
        "OAIPMH_ListRecords_resumptionToken_expirationDate"]

def getWorkerData(rdr_id, worker, mdf): 
    #print("\tLooking up data for %s" % (rdr_id))
    try:
        return worker_output[worker]["ListRecords"][mdf]["data"][rdr_id]
    except KeyError as err:
        print("\tProblem: No Records retrieved for %s (%s)" % (rdr_id, str(err)))
        return None
    
def loadWorkerData(mdf): 
    for worker_file in glob.glob(processed_dir + "/step3_rdr_ListRecords_" + mdf + "_*.json"):
        m = re.split(r".*\/step3_rdr_ListRecords_" 
                + re.escape(mdf)
                + "_(\d+).json$", worker_file)
        worker = m[1]
        with open(worker_file, 'r') as wf:
            worker_output[worker]["ListRecords"][mdf]["data"] = json.load(wf)

with open(input_rdr_csv, 'r') as input_file:
    reader = csv.DictReader(input_file)
    for row in reader:
        data[row["id"]] = row

################################################################################
# ITERATE OVER FUNCTIONAL OAI-PMH RDRS AND RETRIEVE IDENTIFIERS 
################################################################################
for cur_file in glob.glob(processed_dir + "/step3_rdr_*_*_*.json"):
# Collect data about the workers
    print("Scanning %s" % cur_file)
    m = re.split(r'step3_rdr_(\w+)_(\w+)_(\d+).json', cur_file)
    # Example:
    # workers["0"]["Datacite"] = processed_dir/step_3_rdr_Datacite_0.json
    verb    = m[1]
    mdf     = m[2]
    worker  = m[3]
    print("\tFile is for worker %s and mdf %s" % (worker, mdf))
    if not worker in worker_output.keys():
        worker_output[worker] = {}
    if not verb in worker_output[worker].keys():
        worker_output[worker][verb] = {} 
    if not mdf in worker_output[worker][verb].keys(): 
        worker_output[worker][verb][mdf] = {} 
    worker_output[worker][verb][mdf]["file"] = cur_file
    worker_output[worker][verb][mdf]["number_rdrs"] = 0

# We need to know how many workers run for each List* task
workersFor = {
        "ListRecords": {
            "Datacite": 0 }
        }

for verb in workersFor.keys():
    for worker in worker_output:
        for mdf in worker_output[worker][verb].keys():
            workersFor[verb][mdf] += 1 

print("Processing ListRecords %s" % (cur_file))
mdf = "Datacite"
loadWorkerData(mdf)

# Add Values from ListRecords
for rdr_id in data:
    responsibleWorker = str(getHashForRDR(rdr_id, workersFor["ListRecords"][mdf]))
    worker_output[responsibleWorker]["ListRecords"][mdf]["number_rdrs"] += 1
    #print("Processing RDR %s, worker responsible: %s" % (
    #        rdr_id, responsibleWorker))

    data[rdr_id]["OAIPMH_ListRecords_MDF"] = mdf
    listRecordData = getWorkerData(rdr_id, responsibleWorker, mdf)
    if not data[rdr_id]["MDF_" + mdf] or not listRecordData:
        data[rdr_id]["OAIPMH_ListRecords_URL"] = ""
        data[rdr_id]["OAIPMH_ListRecords"] = False
        data[rdr_id]["OAIPMH_ListRecords_chunks"] = 0
        data[rdr_id]["OAIPMH_ListRecords_no_reason"] = ""
        data[rdr_id]["OAIPMH_ListRecords_no_details"] = ""
        data[rdr_id]["OAIPMH_ListRecords_completeListSize"] = 0
        data[rdr_id]["OAIPMH_ListRecords_resumptionToken"] = "" 
        data[rdr_id]["OAIPMH_ListRecords_resumptionToken_expirationDate"] = None
        continue

    cur_rdr_dir = os.path.join(rdrs_dir, rdr_id)

    if not listRecordData["id"] == rdr_id:
        print("Problem: Something is awefully wrong!")
    for key in retrieveFromWorkers:
        data[rdr_id][key] = listRecordData[key]
    numberOfChunks = getNumberOfChunks(cur_rdr_dir, "ListRecords", mdf)
    if numberOfChunks != data[rdr_id]["OAIPMH_ListRecords_chunks"]:
        print("\tProblem: Wrong chunk number in %s: %s (dir) %s (data)" % (
            cur_rdr_dir,
            numberOfChunks,
            data[rdr_id]["OAIPMH_ListRecords_chunks"]))

with open(output_rdr_csv, 'w') as output_file:
    keys = sorted(next(iter(data.values())).keys(),key=str.lower)
    dict_writer = csv.DictWriter(output_file, keys)
    dict_writer.writeheader()
    for next_id in iter(data):
        dict_writer.writerow(data[next_id])
