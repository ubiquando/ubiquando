#!/bin/bash
# Parallelization suggestion: On a 8 Core machine with 16 GB RAM:
# ./step4.sh YYYY-MM-DD 0 11 12
# 13 Workers needed too much RAM
if [ -z $1 ];
then
    echo "Please add date in YYYY-MM-DD format as first parameter of this helper script"
    exit 1
fi

if [ -z $2 ];
then
    echo "Please add the first worker number"
fi

if [ -z $3 ];
then
    echo "Please add the last worker number"
fi

if [ -z $4 ];
then
    echo "Please add total number of workers"
fi

if [ -z $5 ];
then
    images="step4_images.json"
else
    images=$5
fi

log=../data/${1}/log/log_step_4.txt

if [ -e $log ];
then
    mv $log ${log}_bkp
fi

source py3env/bin/activate
echo "Starting $0 at $(date)" >>$log 2>&1
echo "Start Step4 Workers" >>$log

for worker_number in $(seq $2 $3)
do
echo "Start Step4 Worker $worker_number" >>$log
{ time ./step4_processMD.py \
    --date $1 \
    --workers $4 \
    --number $worker_number \
    --images $images & } >>${log}${worker_number} 2>&1
pids[$worker_number]=$!
done

# Wait for workers to finish!
echo "Wait for workers to finish" >>$log
i=$2
for pid in ${pids[*]}
do
    wait $pid;
    echo "===START WORKER $i ===" >>$log
    cat ${log}$i >> $log
    echo "===END WORKER $i ===" >>$log
    rm ${log}$i
    i=$((i+1)) 
done

echo "Start Processing Harvest (Combining Results) at $(date)" >>$log 2>&1
{ time ./step4_combineWorker.py --date $1 --images $images; } >>$log 2>&1

echo "Finishing $0 at $(date)" >>$log 2>&1
