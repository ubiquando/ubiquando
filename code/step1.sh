#!/bin/bash
today=$(date +'%Y-%m-%d')
log=../data/$today/log/log_step_1.txt 
# empty log
mkdir -p ../data/$today/log
source py3env/bin/activate
echo "Starting $0 at $(date)" >>$log
{ time ./step1_retrieveRe3data.py ; } >>$log 2>&1
{ time ./step1_processRe3data.py --date $today ;  } >>$log 2>&1
echo "Finishing $0 at $(date)" >>$log
