#!/bin/bash
# Parallelization suggestion: 
# On a 8 Core machine with 16 GB RAM:
# ./step5.sh YYYY-MM-DD 0 9 10
# 11 Workers needed too much RAM
# On a 4 Core machine with 8 GB RAM:
# ./step5.sh YYYY-MM-DD 0 3 4 
# 5 Workers needed too much RAM

if [ -z $1 ];
then
    echo "Please add date in YYYY-MM-DD format as first parameter of this helper script"
    exit 1
fi

if [ -z $2 ];
then
    echo "Please add the first worker number"
fi

if [ -z $3 ];
then
    echo "Please add the last worker number"
fi

if [ -z $4 ];
then
    echo "Please add total number of workers"
fi

log=../data/${1}/log/log_step_5_$(hostname).txt

source py3env/bin/activate
echo "Starting $0 at $(date)" >>$log 2>&1
echo "Start Step5 Workers" >>$log

for worker_number in $(seq $2 $3)
do
{ time ./step5_retrieveImages.py \
    --date $1 \
    --workers $4 \
    --number $worker_number & } >>${log}${worker_number} 2>&1
pids[$worker_number]=$! 
done

# Wait for workers to finish!
echo "Wait for workers to finish" >>$log
i=$2
for pid in ${pids[*]}
do
    wait $pid;
    echo "===START WORKER $i ===" >>$log
    cat ${log}$i >> $log
    echo "===END WORKER $i ===" >>$log
    rm ${log}$i
    i=$((i+1)) 
done

echo "Finishing $0 at $(date)" >>$log 2>&1
