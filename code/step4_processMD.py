#!/usr/bin/env python3
import csv
from lxml import etree
from lxml import objectify
import json
import os
import sys
import argparse
import pprint
import glob
import rdr
import datacite
import datetime
import urllib.parse
import re
from helper import urlValid, urlFix, suitableMD, isValidDOI, getHashForRDR

################################################################################
# CONFIGURATION AND PREPARATION
################################################################################
parser = argparse.ArgumentParser(
    description='Step 3: Combine the data collected during harvesting'
)
parser.add_argument('--date',
        required    = True,
        help        ="Date when re3data.org data were harvested (Format YYYY-MM-DD)")
parser.add_argument('--workers',
        default     = 1,
        help        ="Number of workers spawned")
parser.add_argument('--number',
        default     = 0,
        help        ="Number of this worker, starting at 0")
parser.add_argument('--images',
        default     = "",
        help        ="Json-formatted file as images input")

args                = parser.parse_args()
date                = args.date
workers             = int(args.workers)
number              = int(args.number)
mime_type_regex     = re.compile('image/[a-zA-Z]+(\s*;.*)?$')

print("Worker %i: Starting processing at %s\n\tworkers: %i\n\tdate: %s" % (
    number,
    datetime.datetime.now().isoformat(),
    workers,
    date))

########################################
# Derived values (do not change)
########################################
rel_base_dir    = "../data/" + date
base_dir        = os.path.abspath(os.path.expanduser(rel_base_dir))
processed_dir   = os.path.join(base_dir, "processed")
raw_dir         = os.path.join(base_dir, "raw")
rdrs_dir        = os.path.join(raw_dir, "rdrs")

input_rdr       = os.path.join(processed_dir, "step3_rdr.csv")
output_rdr      = os.path.join(processed_dir, "step4_rdr.json" + str(number))
output_images   = os.path.join(processed_dir, "step4_images.json" + str(number))

data            = {} 
licenses        = {}

images          = {}

input_images       = os.path.join(processed_dir, args.images)
if os.path.isfile(input_images):
    with open (input_images, 'r') as iif:
        images = json.load(iif)

def is_or_contains_image(md):
    if md.getter("resourceTypeGeneral") == "Image":
        return True 
    if md.getter("formats"):
        for data_format in md.getter("formats"):
            if mime_type_regex.match(data_format):
                return True
    return False

################################################################################
# ITERATE OVER FUNCTIONAL OAI-PMH RDRS AND SELECT BASED ON METADATA 
################################################################################
with open(input_rdr, 'r') as input_file:
    reader = csv.DictReader(input_file)
    num_image = 0

    # We will set default values for all RDRS, the outer loop hence iterates
    # over all RDRs
    for row in reader:
        # If we didn't harvest any Datacite metadata we can skip this RDR
        if row["MDF_Datacite"] == "":
            continue
        if row["OAIPMH_ListRecords_chunks"] == 0:
            print("No metadata records for %s" % row["id"])
            continue

        # This directory includes all metadata harvested in step 3:
        rdr_dir = os.path.join(rdrs_dir, row["id"])
        
        # Metadata are chunked, we iterate over all metadata chunks
        for cur_file in glob.glob(rdr_dir + "/ListRecords_Datacite_*.xml"):
            # we distribute the worker load here
            m = re.split(r'ListRecords_Datacite_(\d+).xml', cur_file)
            if int(m[1]) % workers != number:
                continue
            print("Processing %s" % cur_file)
            chunk = objectify.parse(cur_file)
            root  = chunk.getroot()
            what = "resource"
            # each resource element should include Datacite metadata for an item
            for resource in root.xpath('.//*[local-name() = "' + what + '"]'):
                xml_chunk = etree.tostring(resource).decode("utf-8")
                md = datacite.Datacite(xml_chunk)
                
                # Check whether item is of interest (an image)
                if not is_or_contains_image(md):
                    continue
                # Each worker has its "num_key namespace"
                # worker 0: 0,4,8
                # worker 1: 1,5,9
                # worker 2: 2,6,10
                # worker 3: 3,7,11
                # ...
                num_key = number + (num_image * workers)
                # Check whether item has a valid DOI
                if md.getter("identifier"):
                    key = md.getter("identifier")
                else:
                    print("Warning: %s has no identifier, using num_key (%i)" % (
                        row["id"],
                        num_key )
                    )
                    key = num_key

                num_image = num_image + 1

                images[key] = {}

                # Default values
                images[key]["rdr"]      = row["id"]
                images[key]["num_key"]  = num_key
                images[key]["rtg"]      = md.getter("resourceTypeGeneral")
                images[key]["chrono"]   = ""
                images[key]["geo"]      = ""
                images[key]["lic"]      = ""
                images[key]["formats"]  = ""

                # TEMPORAL COVERAGE
                if md.getter("DateCreated"):
                    images[key]["chrono"] = md.getter("DateCreated")
                else:
                    print("GREPME: No date for %s" % key)

                # SPATIAL COVERAGE
                if md.getter("geolocations"):
                    if len(md.getter("geolocations")) > 0:
                        images[key]["geo"] = ",".join(md.getter("geolocations"))

                # LICENSE
                if md.getter("rightsURI"):
                    if len(md.getter("rightsURI")) > 0:
                        images[key]["lic"] = ",".join(md.getter("rightsURI"))
                else:
                    print("MEGREP: No lic for %s" % key)
        
                # FORMATS
                if md.getter("formats"):
                    image_mimes = []
                    for data_format in md.getter("formats"):
                        if mime_type_regex.match(data_format) and data_format not in image_mimes:
                            image_mimes.append(data_format)
                    images[key]["formats"] = ";".join(image_mimes)

################################################################################
# SAVE OUTPUT 
################################################################################
with open(output_images, 'w') as itf:
    json.dump(images, itf)

print("Worker %i: Finished processing at %s\n\tdate: %s" % (
    number,
    datetime.datetime.now().isoformat(),
    date))
