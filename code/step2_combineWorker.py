#!/usr/bin/env python3
from lxml import etree
from lxml import objectify
import csv
import datetime
import glob
import os
import rdr
import re
import socket
import json
import sys
import urllib.request
import urllib.parse
import pprint
import argparse
from helper import urlValid, getHashForRDR

################################################################################
# CONFIGURATION AND PREPARATION
################################################################################

########################################
# CONFIGURATION AND PREPARATION
########################################
# TODO add criteria, timeout and oaiResponseValidator as argument
parser = argparse.ArgumentParser(
    description='Stage1: Process the data retrieved by re3data.org'
)
parser.add_argument('--date',
        required    = True,
        help        ="Date when re3data.org data were harvested (Format YYYY-MM-DD)")
args = parser.parse_args()

date                = args.date 

########################################
# Derived values (do not change)
########################################
rel_base_dir    = "../data/" + date
base_dir        = os.path.abspath(os.path.expanduser(rel_base_dir))
processed_dir   = os.path.join(base_dir, "processed")
raw_dir         = os.path.join(base_dir, "raw")
rdrs_dir        = os.path.join(raw_dir, "rdrs")

input_rdr_csv   = os.path.join(processed_dir, "step1_rdr.csv")
output_rdr_csv  = os.path.join(processed_dir, "step2_rdr.csv")
output_mdf_csv  = os.path.join(processed_dir, "step2_mdf.csv")

data            = {}
seenMdfs        = {}
worker_output   = {}
verbs           = ["ListMetadataFormats", "Identify"]
mdfMappings     = {
        "http://kpbc.umk.pl/dlibra/attribute-schema.xsd": "MDF_DCMI-Terms",
        "http://www.clarin.eu/cmd/xsd/minimal-cmdi.xsd": "MDF_CMDI",
        "http://www.unidata.ucar.edu/schemas/thredds/InvCatalog.1.0.xsd": "MDF_THREDDS",
        "http://ws02.iula.upf.edu/corpus_data/schemas/me.xsd": "MDF_IULA",
        "http://amf.openlib.org/2001/amf.xsd": "MDF_AMF",
        "http://catalog.clarin.eu/ds/ComponentRegistry/rest/registry/profiles/clarin.eu:cr1:p_1288172614026/xsd": "MDF_OLAC",
        "http://catalog.clarin.eu/ds/ComponentRegistry/rest/registry/profiles/clarin.eu:cr1:p_1290431694579/xsd": "MDF_OLAC",
        "http://catalog.clarin.eu/ds/ComponentRegistry/rest/registry/profiles/clarin.eu:cr1:p_1290431694580/xsd": "MDF_OLAC",
        "http://catalog.clarin.eu/ds/ComponentRegistry/rest/registry/profiles/clarin.eu:cr1:p_1320657629644/xsd": "MDF_OLAC",
        "http://catalog.clarin.eu/ds/ComponentRegistry/rest/registry/profiles/clarin.eu:cr1:p_1336550377513/xsd": "MDF_OLAC",
        "http://catalog.clarin.eu/ds/ComponentRegistry/rest/registry/profiles/clarin.eu:cr1:p_1345180279115/xsd": "MDF_OLAC",
        "http://catalog.clarin.eu/ds/ComponentRegistry/rest/registry/profiles/clarin.eu:cr1:p_1345561703620/xsd": "MDF_OLAC",
        "http://catalog.clarin.eu/ds/ComponentRegistry/rest/registry/profiles/clarin.eu:cr1:p_1345561703673/xsd": "MDF_OLAC",
        "http://catalog.clarin.eu/ds/ComponentRegistry/rest/registry/profiles/clarin.eu:cr1:p_1357720977528/xsd": "MDF_OLAC",
        "http://catalog.clarin.eu/ds/ComponentRegistry/rest/registry/profiles/clarin.eu:cr1:p_1380106710826/xsd": "MDF_OLAC",
        "http://catalog.clarin.eu/ds/ComponentRegistry/rest/registry/profiles/clarin.eu:cr1:p_1386164908461/xsd": "MDF_OLAC",
        "http://cocoon.huma-num.fr/schemas/dcq.xsd": "MDF_DCMI-Terms",
        "http://datacite.org/schema/kernel-4": "MDF_Datacite",
        "http://diglib.hab.de/rules/schema/emblem/emblem-1-2.xsd": "MDF_Emblem",
        "http://docs.rioxx.net/schema/v1.0/rioxx.xsd": "MDF_RIOXX",
        "http://dublincore.org/schemas/xmls/qdc/2003/04/02/qualifieddc.xsd": "MDF_DCMI-Terms",
        "http://dublincore.org/schemas/xmls/qdc/2006/01/06/dcterms.xsd": "MDF_DCMI-Terms",
        "http://schema.datacite.org/meta/nonexistant/nonexistant.xsd": "MDF_Datacite",
        "http://schema.datacite.org/oai/oai-1.0/oai.xsd": "MDF_Datacite",
        "http://dublincore.org/schemas/xmls/qdc/2008/02/11/qualifieddc.xsd": "MDF_DCMI-Terms",
        "http://emblems.let.uu.nl/static/emit/emblem-1-2-hi.xsd": "MDF_Emblem",
        "http://exyus.com/xcs/tasklist/source/?f=put_atom.xsd": "MDF_Atom",
        "http://files.dnb.de/standards/xmetadissplus/xmetadissplus.xsd": "MDF_XmetaDissPlus",
        "http://gams.uni-graz.at/edm/2017-08/EDM.xsd": "MDF_EDM",
        "http://gcmd.gsfc.nasa.gov/Aboutus/xml/dif/dif_v9.4.xsd": "MDF_Dif",
        "http://gcmd.gsfc.nasa.gov/Aboutus/xml/dif/dif_v9.7.xsd": "MDF_Dif",
        "http://gcmd.nasa.gov/Aboutus/xml/dif/dif_v9.8.2.xsd": "MDF_Dif",
        "http://infra.clarin.dk/olac/olac.xsd": "MDF_OLAC",
        "http://irdb.nii.ac.jp/oai/junii2.xsd": "MDF_Junii",
        "http://localhost:8080/openwis-user-portal/xml/schemas/iso19139/schema.xsd": "MDF_ISO19139",
        "http://metashare.ilsp.gr/META-XMLSchema/v2.1/META-SHARE-Resource.xsd": "MDF_MetaShare",
        "http://metashare.ilsp.gr/META-XMLSchema/v3.0/META-SHARE-Resource.xsd": "MDF_MetaShare",
        "http://naca.central.cranfield.ac.uk/ethos-oai/2.0/uketd_dc.xsd": "MDF_UKETDC",
        "http://purl.org/net/oclcterms-recset": "MDF_OCLC",
        "http://schema.datacite.org/meta/kernel-2.2/metadata.xsd": "MDF_Datacite",
        "http://schema.datacite.org/meta/kernel-3/metadata.xsd": "MDF_Datacite",
        "http://schema.datacite.org/meta/nonexistant/nonexistant.xsd": "MDF_Datacite", 
        "http://services.ands.org.au/documentation/rifcs/1.3/schema/registryObjects.xsd": "MDF_ISO2146",
        "http://services.ands.org.au/documentation/rifcs/schema/registryObjects.xsd": "MDF_ISO2146",
        "http://standards.iso.org//ittf/PubliclyAvailableStandards/MPEG-21_schema_files/did/didl.xsd": "MDF_DIDL",
        "http://standards.iso.org/iso/19115/-3/mds/1.0/mds.xsd": "MDF_ISO19115",
        "http://standards.iso.org/ittf/PubliclyAvailableStandards/MPEG-21_schema_files/did/didl.xsd": "MDF_DIDL",
        "http://tweety.lanl.gov/public/schemas/2008-06/atom-tron.sch": "MDF_ORE",
        "http://vo.ari.uni-heidelberg.de/docs/schemata/VOResource-v1.0.xsd": "MDF_VOResource",
        "http://worldcat.org/xmlschemas/qdc/1.0/qdc-1.0.xsd": "MDF_DCMI-Terms",
        "http://ws.pangaea.de/schemas/pangaea/MetaData.xsd": "MDF_Pangaea",
        "http://www.biocatalogue.org/2009/xml/rest/schema-v1.xsd": "MDF_DCMI-Terms",
        "http://www.ddialliance.org/Specification/DDI-Codebook/2.5/XMLSchema/codebook.xsd": "MDF_DDI",
        "http://www.ddialliance.org/Specification/DDI-Lifecycle/3.1/XMLSchema/instance.xsd": "MDF_DDI",
        "http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/instance.xsd": "MDF_DDI",
        "http://www.dspace.org/schema/dim.xsd": "MDF_DIM",
        "http://www.europeana.eu/schemas/ese/ESE-V3.2.xsd": "MDF_ESE",
        "http://www.fgdc.gov/metadata/fgdc-std-001-1998.xsd": "MDF_FGDC",
        "http://www.fgdc.gov/schemas/metadata/fgdc-std-001-1998.xsd": "MDF_FGDC",
        "http://www.isotc211.org/2005/gmd/gmd.xsd": "MDF_GMD",
        "http://www.language-archives.org/OLAC/1.1/olac.xsd": "MDF_OLAC",
        "http://www.loc.gov/METS/": "MDF_METS",
        "http://www.loc.gov/mets/mets.xsd": "MDF_METS",
        "http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd": "MDF_SlimMARC21",
        "https://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd": "MDF_SlimMARC21",
        "http://www.loc.gov/standards/mets/mets.xsd": "MDF_METS",
        "http://www.loc.gov/standards/mets/version16/mets.xsd": "MDF_METS",
        "http://www.loc.gov/standards/mets/version16/mets.xsd": "MDF_METS",
        "http://www.loc.gov/standards/mets/version17/mets.v1-7.xsd": "MDF_METS",
        "http://www.loc.gov/standards/mods/v3/Mods-3-3.xsd": "MDF_MODS",
        "http://www.loc.gov/standards/mods/v3/mods-3-0.xsd": "MDF_MODS",
        "http://schema.datacite.org/oai/oai-1.0/oai.xsd": "MDF_Datacite",
        "https://schema.datacite.org/oai/oai-1.0/oai.xsd": "MDF_Datacite",
        "http://www.loc.gov/standards/mods/v3/mods-3-1.xsd": "MDF_MODS",
        "http://www.loc.gov/standards/mods/v3/mods-3-3.xsd": "MDF_MODS",
        "http://www.loc.gov/standards/mods/v3/mods-3-4.xsd": "MDF_MODS",
        "http://www.loc.gov/standards/mods/v3/mods-3-5.xsd": "MDF_MODS",
        "http://www.loc.gov/standards/mods/v3/mods-3-6.xsd": "MDF_MODS",
        "http://www.lyncode.com/schemas/xoai.xsd": "MDF_XOAI",
        "http://www.mpi.nl/IMDI/schemas/xsd/IMDI_3.0.xsd": "MDF_IMDI",
        "http://www.ndltd.org/standards/metadata/etdms/1-0/etdms.xsd": "MDF_ETDMS",
        "http://www.ndltd.org/standards/metadata/etdms/1.0/etdms.xsd": "MDF_ETDMS",
        "http://www.openarchives.org/OAI/1.1/dc.xsd": "MDF_DublinCore",
        "http://www.openarchives.org/OAI/2.0/dc.xsd": "MDF_DublinCore",
        "http://www.openarchives.org/OAI/2.0/oai_dc.xsd": "MDF_DublinCore",
        "http://www.openarchives.org/OAI/2.0/oai_dcterms.xsd": "MDF_DCMI-Terms",
        "http://www.openarchives.org/OAI/2.0/rdf.xsd": "MDF_RDF",
        "http://www.persistent-identifier.de/xepicur/version1.0/xepicur.xsd": "MDF_Epicur",
        "http://www.rioxx.net/schema/v2.0/rioxx/rioxx.xsd": "MDF_RIOXX",
        "http://www.w3.org/ns/dcat#": "MDF_DCAT",
        "https://api.figshare.com/v2/static/figshare-oai-qdc.xsd": "MDF_DCMI-Terms",
        "https://infra.clarin.eu/CMDI/1.1/xsd/minimal-cmdi.xsd": "MDF_CMDI",
        "https://infra.clarin.eu/CMDI/1.x/xsd/cmd-envelop.xsd": "MDF_CMDI",
        "https://resources.bepress.com/assets/xsd/oai_qualified_dc.xsd": "MDF_DCMI-Terms",
        "https://schema.datacite.org/meta/kernel-4.0/metadata.xsd": "MDF_Datacite",

        "https://www.openaire.eu/cerif_schema/cerif-1.6-2_openaie-1.0.xsd": "MDF_CERIF"
}

retrieveFromWorkers = [
        "",
        "_URL",
        "_no_reason",
        "_no_details"
]

################################################################################
# HELPER FUNCTIONS
################################################################################ 
def getCanonicalMdfName(mdf):
    if mdf in mdfMappings.keys():
        return mdfMappings[mdf]
    else:
        print("OTHER:%s" % mdf)
        return "MDF__" + mdf 

def loadWorkerData(verb):
    for worker_file in glob.glob(processed_dir + "/step2_rdr_" + verb + "_*.json"):
        m = re.split(r".*\/step2_rdr_" 
                + re.escape(verb)
                + "_(\d+).json$", worker_file)
        pprint.pprint(m)
        worker = m[1]
        with open(worker_file, 'r') as wf:
            worker_output[worker][verb]["data"] = json.load(wf)

def getWorkerData(rdr_id, worker, verb): 
    print("\tLooking up data for %s" % (rdr_id))
    try:
        return worker_output[worker][verb]["data"][rdr_id]
    except KeyError as err:
        print("\tProblem: No Records retrieved for %s (%s)" % (rdr_id, str(err)))
        return None

def evaluateMdfFor(mdf, what):
    if mdf in ["DublinCore", "METS", "UKETDC", "OLAC"]:
        return "E"
    if mdf == "RDF":
        return "-"
    if mdf == "DIDL":
        if what in ["Retrieval", "Image"]:
            return "E"
        if what in ["Identifier", "License"]:
            return "E,U"
        else:
            return "-"
    if mdf == "MODS":
        if what in ["Temporal", "Identifier", "Retrieval"]:
            return "E,U"
        elif what == "Spatial":
            return "-"
        else:
            return "E"
    if mdf == "DCMI-Terms":
        if what == "Identifier":
            return "E,U"
        else:
            return "E"
    if mdf == "SlimMARC21":
        if what == "Temporal":
            return "E,U"
        if what == "Retrieval":
            return "-"
        else:
            return "E"
    if mdf == "Datacite":
        if what in ["Identifier", "Retrieval"]:
            return "E,M,U"
        else:
            return "E,U"
    if mdf == "ETDMS":
        if what in ["Identifier", "Retrieval", "Temporal"]:
            return "E"
        else:
            return "-"
    return "?"


################################################################################
# MAKE A SELECTION, CHECK AND ENRICH IT, AND PERSIST IT AS A STAGE 2 RESULT
################################################################################ 
for cur_file in glob.glob(processed_dir + "/step2_rdr_*_*.json"):
# Collect data about the workers
    m = re.split(r'step2_rdr_(\w+)_(\d+).json', cur_file)
    # Example:
    # workers["0"]["DublinCore"] = processed_dir/step_3_rdr_DublinCore_0.json
    verb    = m[1] 
    worker  = m[2]
    print("Scanning %s" % cur_file)
    print("\tFile is for worker %s and verb %s" % (worker, verb))
    if not worker in worker_output:
        worker_output[worker] = {}
    worker_output[worker][verb] = {}
    worker_output[worker][verb]["file"] = cur_file 
    worker_output[worker][verb]["number_rdrs"] = 0

for verb in verbs:
    loadWorkerData(verb)

workersFor = { 
        "ListMetadataFormats": 0,
        "Identify": 0
}

for verb in workersFor.keys():
    for worker in worker_output: 
            workersFor[verb] += 1 

pprint.pprint(workersFor)
with open(input_rdr_csv, 'r') as input_file:
    reader = csv.DictReader(input_file)
    for row in reader:
        # default settings for data to be enriched
        data[row["id"]]                                             = row
        data[row["id"]]["OAIPMH_Identify_deletedRecord"]            = "unknown"
        for verb in verbs: 
            data[row["id"]]["OAIPMH_" + verb ]                      = False
            data[row["id"]]["OAIPMH_" + verb + "_URL"]              = "" 
            data[row["id"]]["OAIPMH_" + verb + "_no_details"]       = ""
            data[row["id"]]["OAIPMH_" + verb + "_no_reason"]        = "" 
        if row["OAIPMH_URL_canonical"] == "True":
            workersMdf = str(getHashForRDR(row["id"], workersFor["ListMetadataFormats"]))
            workersIde = str(getHashForRDR(row["id"], workersFor["Identify"]))

            print("Processing RDR %s" % (row["id"]))
            rdr_dir = os.path.join(rdrs_dir, row["id"])
            # ListMetadataFormats
            listMetadataFormatsData = getWorkerData(row["id"], workersMdf, "ListMetadataFormats") 
            
            if not listMetadataFormatsData["id"] == row["id"]:
                print("Problem: Something is awefully wrong!")
            for key in retrieveFromWorkers:
                data[row["id"]]["OAIPMH_ListMetadataFormats" + key] = listMetadataFormatsData["OAIPMH_ListMetadataFormats" + key]
            if data[row["id"]]["OAIPMH_ListMetadataFormats"]:
                oairesponseFile = os.path.join(rdr_dir, "ListMetadataFormats_0.xml")
                oairesponse = objectify.parse(oairesponseFile)
                mdfs = oairesponse.getroot().ListMetadataFormats.getchildren()
                mdfsForRdr = []
                for mdfElement in mdfs:
                    schema = "" + mdfElement.schema
                    schema = schema.strip()
                    if not urlValid(schema) or ' ' in schema:
                        print("Problem: No valid url, ignoring mdf %s" % schema)
                        continue
                    # Some RDRs tell us more than once, that they support a mdf
                    canonicalMdfName = getCanonicalMdfName(schema)
                    if canonicalMdfName in mdfsForRdr:
                        continue
                    mdfsForRdr.append(canonicalMdfName)
                    if canonicalMdfName not in seenMdfs.keys():
                        seenMdfs[canonicalMdfName] = 1
                    else:
                        seenMdfs[canonicalMdfName] += 1
                    # __ will be in return value of getCanonicalMdfName, if there are not
                    # many RDRs supporting this scheme. If this changes, the MDF file still
                    # has the total number and the code must be updated accordingly to add
                    # a canonical name and therefore add it to the RDR statistics.
                    if "__" not in canonicalMdfName:
                        data[row["id"]][canonicalMdfName] = mdfElement.metadataPrefix
                        data[row["id"]][canonicalMdfName + "_schema"] = schema
                    print("%s supports mdf %s" % (row["id"], canonicalMdfName))
                data[row["id"]]["OAIPMH_ListMetadataFormats"] = True

            # Identify 
            
            identifyData = getWorkerData(row["id"], workersIde, "Identify")
            if not identifyData["id"] == row["id"]:
                print("Problem: Something is awefully wrong!")
            for key in retrieveFromWorkers:
                data[row["id"]]["OAIPMH_Identify" + key] = identifyData["OAIPMH_Identify" + key]
            if data[row["id"]]["OAIPMH_Identify"]:
                oairesponseFile = os.path.join(rdr_dir, "Identify_0.xml")
                oairesponse = objectify.parse(oairesponseFile)
                data[row["id"]]["OAIPMH_Identify_deletedRecord"] = (
                    oairesponse.getroot().Identify.deletedRecord)


# ENRICH 3: Mark all mdfs as False if the rdr does not support them
for rdr_id in data:
    for mdf in seenMdfs.keys():
        if "__" in mdf:
            continue
        try:
            if data[rdr_id][mdf]:
                pass
        except KeyError:
            data[rdr_id][mdf] = ""
        try:
            if data[rdr_id][mdf + "_schema"]:
                continue
        except KeyError:
            data[rdr_id][mdf + "_schema"] = ""

# ENRICH 4: Add Evaluation to mdf-file
orderedMdfs = [(key, seenMdfs[key]) for key in sorted(seenMdfs, key=seenMdfs.get, reverse=True)]
printMdfs = []
evaluateFor = ["Identifier", "Retrieval", "License", "Temporal", "Spatial", "Image"]
for mdf in orderedMdfs:
    row = []
    formatName = mdf[0].split("MDF_")[1] 
    row.append(formatName) 
    row.append(mdf[1]) 
    for what in evaluateFor:
        row.append(evaluateMdfFor(formatName, what))
    printMdfs.append(row)

########################################
# PERSIST
########################################
with open(output_rdr_csv, 'w') as output_file:
    keys = sorted(next(iter(data.values())).keys(),key=str.lower)
    dict_writer = csv.DictWriter(output_file, keys)
    dict_writer.writeheader()
    for next_id in iter(data):
        dict_writer.writerow(data[next_id])

with open(output_mdf_csv, 'w') as output_file:
    dict_writer = csv.writer(output_file)
    dict_writer.writerow(["Metadata Scheme", "RDRs"] + evaluateFor)
    for row in printMdfs: 
        dict_writer.writerow(row)
