#!/usr/bin/env python3
from lxml import etree
from lxml import objectify
import pprint
import io 
import sys
import datetime
import re
import os
from helper import isValidDOI, urlValid 

deleteTags = [
        "alternateIdentifiers",
        "titles",
        "contributors",
        "creators",
        "publisher",
        "publicationYear",
        "language",
        "descriptions",
        "subject",
        "subjects"
]

DEBUG = False

class Datacite(object):
    def __init__(self, string):
        self.string = string 
        self.createFromString()

    def createFromString(self):
        fileobject  = io.StringIO(self.string)
        self.tree   = objectify.parse(fileobject)
        self.root   = self.tree.getroot()
        self.ns     = self.root.nsmap[None]
        self.values = {}

        # validate
        if DEBUG == True:
            kernel = self.ns.split("/")[-1]
            xsd_path = "./util/" + kernel + "/metadata.xsd"
            if not os.path.isfile(xsd_path):
                print("ERROR! %s has no schema!" % xsd_path)
            else:
                xmlschema_doc = etree.parse(xsd_path)
                xmlschema = etree.XMLSchema(xmlschema_doc)
                print(xmlschema.validate(self.tree))

        #print(etree.tostring(self.root, pretty_print=True).decode("utf-8"))
        for element in self.root.getchildren(): 
            elementTag = element.tag.split("}")[1]
            self.values[elementTag] = str(element)
            if elementTag in deleteTags:
                del self.values[elementTag]
            if elementTag == "identifier":
                if not isValidDOI(str(element)):
                    print("%s is not a valid DOI" % (str(element)))
                    self.values["identifier"] = ""
                    self.values["identifierType"] = str(element.get("identiferType"))
            if elementTag == "geoLocations": 
                #print(etree.tostring(element))
                self.values["geolocationsAsString"] = (
                        etree.tostring(element))
                self.values["geolocations"] = []
                gl_strings = []
                for gl in element.getchildren():
                    for gl_item in gl.getchildren():
                        gl_itemTag = gl_item.tag.split("}")[1]
                        gl_strings.append("%s: %s" % (gl_itemTag, str(gl_item)))
                    gl_string = ",".join(gl_strings)
                    if len(gl_strings) > 0:
                        self.values["geolocations"].append(gl_string)
                if len(self.values["geolocations"]) == 0:
                    self.values["geolocationsAsString"] = ""
            if elementTag == "resourceType":
                self.values["resourceTypeGeneral"] = str(element.get("resourceTypeGeneral"))
                if not self.values[elementTag]:
                    del self.values[elementTag]
            if elementTag == "formats":
                del self.values["formats"]
                self.values["formats"] = []
                for format in element.getchildren():
                    self.values["formats"].append(str(format))
            if elementTag == "dates":
                for date in element.getchildren():
                    dateType = str(date.get("dateType"))
                    if re.search("created", dateType, re.IGNORECASE):
                        print("GREPME: %s is %s for %s" % (dateType, str(date), self.string))
                        self.values["DateCreated"] = str(date)
            if elementTag == "rightsList":
                del self.values["rightsList"]
                self.values["rights"] = []
                self.values["rightsURI"] = []
                for right in element.getchildren():
                    if right.get("rightsURI"):
                        self.values["rights"].append(str(right))
                        self.values["rightsURI"].append(str(right.get("rightsURI")))
                if len(self.values["rights"]) == 0 and len(self.values["rightsURI"]) == 0:
                    del self.values["rights"]
                    del self.values["rightsURI"]
                    print("MEGREP: No rigths: %s" % sel.values["rightsList"])

    def getter(self, what):
        try:
            return self.values[what]
        except KeyError:
            return None 

    def __str__(self):
        return self.string
