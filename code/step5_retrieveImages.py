#!/usr/bin/env python3
import os
import sys
import argparse
import pprint
import glob
import datacite
import requests
import datetime
import json
import re
from lxml import objectify
from lxml import etree
from helper import urlValid, isValidDOI, getHashForImage

################################################################################
# CONFIGURATION AND PREPARATION
################################################################################
parser = argparse.ArgumentParser(
    description='Step 5: Retrieve the selected data'
)
parser.add_argument('--date',
        required    = True,
        help        ="Date when re3data.org data were harvested (Format YYYY-MM-DD)")
parser.add_argument('--workers',
        default     = 1,
        help        ="Number of workers spawned")
parser.add_argument('--number',
        default     = 0,
        help        ="Number of this worker, starting at 0")

args = parser.parse_args()

date                = args.date
workers             = int(args.workers)
number              = int(args.number)
cache_after         = 100

mime_type_regex     = re.compile('image/[a-zA-Z]+(\s*;.*)?$')
headers             = { 'Accept': 'image/*' }

########################################
# Derived values (do not change)
########################################
rel_base_dir    = "../data/" + date
base_dir        = os.path.abspath(os.path.expanduser(rel_base_dir))
processed_dir   = os.path.join(base_dir, "processed")
raw_dir         = os.path.join(base_dir, "raw")
items_dir       = os.path.join(raw_dir, "items")

input_images    = os.path.join(processed_dir, "step4_images.json")
output_images   = os.path.join(processed_dir, "step5_images.json" + str(number))

images          = {}
images_cached   = {}

# The payload of a link header is formatted like this
# (square brackets delimit variables):
# <[url]>;rel=[rel_type];(type="[mime_type]"){0,1}, ...
# We will parse it in a way that we have a list of links like that:
# [ { 'url': [url], 'rel': [rel_type], 'type': [mime_type] } , ... ]
def parse_links(link_string):
    link_list = []
    for cur_link in link_string.split(","):
        link_as_strings = cur_link.strip().split(";")
        # The first ist always the URL
        link = {'url' : link_as_strings[0].strip('<>') }
        # We don't need the URL anymore in the string
        link_as_strings.pop(0)
        for link_info in link_as_strings:
            link_info_as_strings = link_info.split("=")
            link[link_info_as_strings[0]] = link_info_as_strings[1].strip('"')
        link_list.append(link)
    return link_list


################################################################################
# ITERATE OVER IMAGES AND CHECK RETRIEVABILITY
################################################################################
with open(input_images, 'r') as images_file:
    images = json.load(images_file)

if os.path.isfile(output_images):
    with open(output_images, 'r') as cache_file:
        images_cached = json.load(cache_file)

print("Worker %i: Starting processing at %s\n\tworkers: %i\n\tdate: %s" % (
    number,
    datetime.datetime.now().isoformat(),
    workers,
    date))

imagesProcessed = 0

for key, cur_image in images.items():
    # worker is not responsible
    if not getHashForImage(key, workers) == number:
        continue

    # already checked, in cache
    if key in images_cached.keys():
        print("%s is in cache! Skipping" % key)
        continue

    image = cur_image
    image["ret"] = ""
    
    if not isValidDOI(key):
        print("%s is not a valid DOI!" % key)
        images_cached[key] = image
        imagesProcessed = imagesProcessed + 1 
        continue
    else:
        doi_url = "https://doi.org/" + key
        print("HTTP HEAD to %s" % doi_url)
        try:
            r = requests.head(doi_url, allow_redirects=True, headers=headers)
        except Exception as e:
            print("\tERROR: %s" % str(e))
            
        if r.status_code == 200:
            try:
                print("\tContent-type of DOI-resolution: %s" % r.headers["content-type"])
            except KeyError as e:
                print("Error, no Content-type: %s", str(e))
                for h in r.headers:
                    print(h)
                continue
            if mime_type_regex.match(r.headers["content-type"]):
                images[key]["ret"] = doi_url
            # try evaluate Link header! 
            if "Link" in r.headers.keys():
                for link in parse_links(r.headers["link"]):
                    if "type" not in link.keys():
                        continue 
                    if re.match(mime_type_regex, link["type"]):
                        print("\tHTTP HEAD to %s with mime-type %s" % (link["url"], link["type"])) 
                        # check whether the mime_type is in the
                        try:
                            r2 = requests.head(link["url"],
                                allow_redirects=True,
                                headers= { 'Accept' : link["type"] }
                            )
                        except Exception as e:
                            print("\tERROR: %s" % str(e))
                        if r2.status_code == 200 and mime_type_regex.match(r2.headers["content-type"]): 
                            images[key]["ret"] = link["url"]
        else:
            print("\tStatus Code is not 200: '%s'" % r.status_code)
        images_cached[key] = image
        imagesProcessed = imagesProcessed + 1 
        # Cache the result each cache_after retrievals
        if (imagesProcessed % cache_after) == 0:
            with open(output_images, 'w') as itf:
                json.dump(images_cached, itf)


################################################################################
# SAVE OUTPUT
################################################################################
with open(output_images, 'w') as itf:
    json.dump(images_cached, itf)
