#!/usr/bin/env python3
import csv
import datetime
import glob
import os
import rdr
import pprint
import argparse
import re

################################################################################
# CONFIGURATION AND PREPARATION
################################################################################
parser = argparse.ArgumentParser(
    description='Stage1: Process the data retrieved by re3data.org'
)
parser.add_argument('--date',
        required    = True,
        help        ="Date when re3data.org data were harvested (Format YYYY-MM-DD)")
args = parser.parse_args()

date            = args.date
blacklist = [
    "r3d100011914" # does not use resumptionToken for over 100.000 items...
]


########################################
# Derived values (do not change)
########################################
rel_base_dir    = "../data/" + date
base_dir        = os.path.abspath(os.path.expanduser(rel_base_dir))
raw_dir         = os.path.join(base_dir, "raw")
rdrs_dir        = os.path.join(raw_dir, "rdrs")
processed_dir   = os.path.join(base_dir, "processed")

output_rdr_csv          = os.path.join(processed_dir, "step1_rdr.csv")
output_api_csv          = os.path.join(processed_dir, "step1_api.csv")
data                    = {}
seenApis                = {} # keep track what kind of APIs are registered
alreadUsedOAIPMHUrls    = {} # keep track which API-URLs are already processed 
                             # this is necessary since some RDRs share an API

createDirs = [ processed_dir ]

for directoryToBeCreated in createDirs:
    try:
        os.stat(directoryToBeCreated)
    except:
        os.mkdir(directoryToBeCreated)
########################################
# HELPER FUNCTIONS
########################################
def getCanonicalApiName(api):
    return "API_" + api

################################################################################
# ITERATE OVER RDRS AND CREATE STAGE 1 RESULT (ID, QUALITY AND APIS)
################################################################################
for cur_file in glob.glob(rdrs_dir + "/r3d*/r3d*"):
    cur_rdr = rdr.RDR(file = cur_file)
    cur_id  = cur_rdr.getId()
    print("Processing RDR %s" % (cur_id))
    data[cur_id] = {}
    data[cur_id]["id"]                      = cur_id
    data[cur_id]["R3D_qm"]                  = cur_rdr.getQualityManagement()
    data[cur_id]["R3D_dataProvider"]        = cur_rdr.isProvider("dataProvider")
    data[cur_id]["R3D_serviceProvider"]     = cur_rdr.isProvider("serviceProvider") 
    data[cur_id]["R3D_name"]                = cur_rdr.getName()
    data[cur_id]["OAIPMH_URL_canonical"]    = False
    data[cur_id]["OAIPMH_URL_corrected"]    = False
    data[cur_id]["OAIPMH_URL_blacklisted"]  = False 
    if not data[cur_id]["R3D_qm"]:
        data[cur_id]["R3D_qm"]              = "unknown"
    data[cur_id]["R3D_certified"]           = False
    if (len(cur_rdr.certificates.keys()) > 0):
        data[cur_id]["R3D_certified"]       = True

    for api in cur_rdr.apis:
        canonical_api = getCanonicalApiName(api)
        if canonical_api not in seenApis.keys():
            seenApis[canonical_api] = 1
        else:
            seenApis[canonical_api] += 1
        data[cur_id][canonical_api]     = True
        data[cur_id][canonical_api + "_URL"] = str(cur_rdr.getApi(api))
        if canonical_api == "API_OAI-PMH": 
            data[cur_id]["OAIPMH_URL_canonical"]    = True 
            # Some RDRs share a OAI-PMH API. We only want to harvest one
            if cur_rdr.getApi(api) in alreadUsedOAIPMHUrls.keys():
                data[cur_id]["OAIPMH_URL_canonical"] = False
            # Some OAI-PMH URLs are registered with a query string.
            # (already including a verb). We don't like that
            if re.search(r'\?', data[cur_id]["API_OAI-PMH_URL"]):
                print("\tDropping query string for %s" % data[cur_id]["API_OAI-PMH_URL"])
                data[cur_id]["API_OAI-PMH_URL"] = (
                        data[cur_id]["API_OAI-PMH_URL"].split("?")[0])
                print("\tNew query string:         %s" % data[cur_id]["API_OAI-PMH_URL"])
                data[cur_id]["OAIPMH_URL_corrected"] = False
            if cur_id in blacklist:
                data[cur_id]["API_OAI-PMH"]     = True
                data[cur_id]["OAIPMH_URL_blacklisted"]  = True
                continue
        alreadUsedOAIPMHUrls[cur_rdr.getApi(api)] = cur_id
    data[cur_id]["R3D_noOfApis"]            = len(cur_rdr.apis)

# Fill in False if API is not supported:
for rdr_id in data:
    for api in seenApis.keys():
        try:
            if data[rdr_id][api]:
                continue
        except KeyError:
            data[rdr_id][api]           = False
            data[rdr_id][api + "_URL"]  = ""

########################################
# PERSIST
########################################
with open(output_rdr_csv, 'w') as output_file:
    keys = sorted(next(iter(data.values())).keys(),key=str.lower)
    dict_writer = csv.DictWriter(output_file, keys)
    dict_writer.writeheader()
    for next_id in iter(data):
        dict_writer.writerow(data[next_id])

printApis = [(key, seenApis[key]) for key in sorted(seenApis, key=seenApis.get, reverse=True)]
with open(output_api_csv, 'w') as output_file:
    dict_writer = csv.writer(output_file)
    dict_writer.writerow(["API", "RDRs", "Adoption"])
    for row in printApis:
        api = row[0].split("API_")[1]
        dict_writer.writerow([api,row[1],"{:.2f} %".format(row[1]/len(data)*100)])
