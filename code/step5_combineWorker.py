#!/usr/bin/env python3 
from lxml import etree
from lxml import objectify
import csv
import json 
import os 
import sys
import argparse
import pprint
import glob
import re
import rdr
from helper import getHashForImage, isValidDOI

################################################################################
# CONFIGURATION AND PREPARATION
################################################################################
parser = argparse.ArgumentParser(
    description='Step 4: Combine the data collected during harvesting'
)
parser.add_argument('--date',
        required    = True,
        help        ="Date when re3data.org data were harvested (Format YYYY-MM-DD)")
parser.add_argument('--worker',
        required    = True,
        help        ="Date when re3data.org data were harvested (Format YYYY-MM-DD)")
args = parser.parse_args() 

date                = args.date
worker              = args.worker
qc_names            = ["ret", "lic", "chrono", "geo"]

########################################
# Derived values (do not change)
########################################
rel_base_dir    = "../data/" + date
base_dir        = os.path.abspath(os.path.expanduser(rel_base_dir))
processed_dir   = os.path.join(base_dir, "processed")
raw_dir         = os.path.join(base_dir, "raw")
rdrs_dir        = os.path.join(raw_dir, "rdrs")

input_rdr       = os.path.join(processed_dir, "step3_rdr.csv")
output_rdr      = os.path.join(processed_dir, "step5_rdr.csv")
input_images    = os.path.join(processed_dir, "step4_images.json")
output_images   = os.path.join(processed_dir, "step5_images.json")
output_images_csv = os.path.join(processed_dir, "step5_images.csv")
output_qc       = os.path.join(processed_dir, "step5_qc.csv")

images_step_4     = {}
images_step_5     = {}
data_rdr          = {}
weights           = {}

def loadWorkerData():
    for worker_file in glob.glob(processed_dir + "/step5_images.json*"):
        regex = r".*\/step5_images.json(\d+)$"
        if not re.match(regex, worker_file):
            continue
        m = re.split(regex, worker_file)
        with open(worker_file, 'r') as wf:
            images_step_5.update(json.load(wf))

def score_fixed(image):
    score = 0
    for key in qc_names:
        if image[key] != "": 
            score += 1 / len(qc_names)
    return score

def score_relative(image, qc):
    score = 0
    for key in qc_names:
        if image[key] != "": 
            score += qc[key]["weight"] 
    return score

########################################
# LOAD INPUT DATA FROM STEP 4
########################################

with open(input_rdr, 'r') as input_rdr_file:
    reader = csv.DictReader(input_rdr_file)
    for row in reader:
        for qc in qc_names:
            row["QC_items_" + qc] = 0 
        row["QC_items_of_interest"] = 0
        row["QC_acc_fixed"] = 0
        row["QC_acc_relative"] = 0 
        if not "R3D_name" in row.keys():
            rdr_file = os.path.join(rdrs_dir, row["id"], row["id"])
            cur_rdr = rdr.RDR(file = rdr_file)
            row["R3D_name"] = cur_rdr.getName()
        data_rdr[row["id"]] = row

with open(input_images, 'r') as input_images_file:
    images_step_4 = json.load(input_images_file)

########################################
# LOAD INPUT DATA FROM STEP 5
######################################## 
if os.path.isfile(output_images):
    with open(output_images, 'r') as output_images_file:
        images_step_5 = json.load(output_images_file)
else:
    loadWorkerData()
    with open(output_images, 'w') as output_images_file:
        json.dump(images_step_5, output_images_file)

########################################
# CHECK IMAGES FILE 
########################################
len_images_step_4 = len(images_step_4.keys())
len_images_step_5 = len(images_step_5.keys())

print("Keys in Step 4: %i" % len_images_step_4 )
print("Keys in Step 5: %i" % len_images_step_5 )

# Try to correct an error due to a former bug in step5_retrieveImages
if len_images_step_4 != len_images_step_5:
    cache_result = False
    images_with_unvalid_doi = 0
    for key in images_step_4.keys():
        if not isValidDOI(key) and key not in images_step_5.keys():
            cache_result = True
            images_step_5[key] = images_step_4[key]
            images_step_5[key]["ret"] = ""
            if images_with_unvalid_doi % 10 == 0:
                print("Repairing %s" % key, end="")
            elif images_with_unvalid_doi % 10 == 9:
                print(" %s" % key)
            else:
                print(" %s" % key, end="")
            images_with_unvalid_doi += 1
    if cache_result:
        print("\n%i images added due to unvalid DOI bug" % images_with_unvalid_doi)
        print("Storing repaired images_step_5")
        with open(output_images, 'w') as output_images_file:
            json.dump(images_step_5, output_images_file)
        len_images_step_4 = len(images_step_4.keys())
        len_images_step_5 = len(images_step_5.keys())
        print("Keys in Step 4: %i" % len_images_step_4 )
        print("Keys in Step 5: %i" % len_images_step_5 )

# Detect the error
if len_images_step_4 != len_images_step_5:
    print("Something went terribly wrong in the image retrieval")
    for key in images_step_4.keys():
        if key not in images_step_5.keys():
            responsible_worker = getHashForImage(key, int(worker))  
            print("Key %s is not in images_step_5 (worker: %s)" % (
                key, responsible_worker ))
    for key in images_step_5.keys():
        if key not in images_step_4.keys():
            responsible_worker = getHashForImage(key, int(worker))  
            print("Key %s is not in images_step_4 (worker: %s)" % (
                key, responsible_worker ))

########################################
# CALCULATE RARENESS AND WEIGHTS
########################################
print("Start calculating item count, rareness and weights")
quality_criteria = {}

for key in qc_names:
    quality_criteria[key] = {
        "items": 0,
        "rareness": 0,
        "weight" : 0,
        "key" : key
        }

items_evaluated = 0
for key in images_step_5.keys():
    if "rtg" not in images_step_5[key].keys():
        images_step_5[key]["rtg"] = images_step_4[key]["rtg"]
    data_rdr[images_step_5[key]["rdr"]]["QC_items_of_interest"] += 1
    items_evaluated += 1 
    for qc in quality_criteria.keys():
        if qc not in images_step_5[key].keys():
            print(getHashForImage(key, int(worker)))
            continue
        if images_step_5[key][qc] != "":
            quality_criteria[qc]["items"] += 1
            data_rdr[images_step_5[key]["rdr"]]["QC_items_" + qc] += 1

total_rareness = 0
for key in qc_names:
    if quality_criteria[key]["items"] > 0:
        quality_criteria[key]["rareness"] = 1 - quality_criteria[key]["items"]/len(images_step_5)
        total_rareness += quality_criteria[key]["rareness"]

for key in qc_names:
    if quality_criteria[key]["items"] > 0:
        quality_criteria[key]["weight"] = quality_criteria[key]["rareness"]/total_rareness

########################################
# CALCULATE METRICS
########################################
print("Start calculating metrics")

for key in images_step_5.keys():
    images_step_5[key]["score_fixed"] = score_fixed(images_step_5[key])
    images_step_5[key]["score_relative"] = score_relative(images_step_5[key], quality_criteria)
    data_rdr[images_step_5[key]["rdr"]]["QC_acc_fixed"] += images_step_5[key]["score_fixed"] 
    data_rdr[images_step_5[key]["rdr"]]["QC_acc_relative"] += images_step_5[key]["score_relative"]

################################################################################
# SAVE OUTPUT 
################################################################################
print("Persist new data")
with open(output_images, 'w') as itf:
    json.dump(images_step_5, itf)

with open(output_images_csv, 'w') as icsv:
    keys = sorted(next(iter(images_step_5.values())).keys(),key=str.lower)
    dict_writer = csv.DictWriter(icsv, keys)
    dict_writer.writeheader()
    for next_id in iter(images_step_5):
        dict_writer.writerow(images_step_5[next_id])

with open(output_rdr, 'w') as output_file:
    keys = sorted(next(iter(data_rdr.values())).keys(),key=str.lower)
    dict_writer = csv.DictWriter(output_file, keys)
    dict_writer.writeheader()
    for next_id in iter(data_rdr):
        dict_writer.writerow(data_rdr[next_id])

with open(output_qc, 'w') as oqc_file:
    keys = sorted(next(iter(quality_criteria.values())).keys(),key=str.lower)
    dict_writer = csv.DictWriter(oqc_file, keys)
    dict_writer.writeheader()
    for next_id in iter(quality_criteria):
        dict_writer.writerow(quality_criteria[next_id])
