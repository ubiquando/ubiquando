#!/usr/bin/env python3
from lxml import objectify
from pprint import pprint

class RDR(object):

    def __init__(self, *args, **kwargs):
        if "file" in kwargs:
            self.file = kwargs.get("file")
        self.createFromFile()

    def createFromFile(self):
        # TODO open not ncessary
        with open(self.file, 'r+') as f:
            self.tree           = objectify.parse(self.file)
            self.root           = self.tree.getroot()
            self.apis           = self.retrieveApis()
            self.certificates   = self.retrieveCertificates()

    def getId(self):
        return str(self.root.repository.findall(
            "{http://www.re3data.org/schema/2-2}re3data.orgIdentifier")[0])

    def getName(self):
        return str(self.root.repository.repositoryName).strip()

    def isProvider(self, pt):
        for providerType in self.root.xpath('.//*[local-name() = "providerType"]'):
            if providerType == pt:
                return True
            return False

    def retrieveApis(self):
        retval = {}
        for api in self.root.repository.findall(
                "{http://www.re3data.org/schema/2-2}api"):
            retval[api.get("apiType")] = api
        return retval

    def hasApi(self, api):
        return api in self.apis.keys()

    def getApi(self, api):
        try:
            return self.apis[api]
        except KeyError:
            return None

    def retrieveCertificates(self):
        retval = {}
        for certificate in self.root.repository.findall(
                "{http://www.re3data.org/schema/2-2}certificate"):
            retval[certificate] = True
        return retval

    def hasCertificate(self, certificate):
        try:
            return self.certificates[certificate]
        except KeyError:
            return False

    def getQualityManagement(self):
        try:
            return self.root.repository.qualityManagement
        except AttributeError:
            return "unknown"
