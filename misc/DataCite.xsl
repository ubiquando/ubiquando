<?xml version="1.0" ?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:dc="http://datacite.org/schema/kernel-4">
  <xsl:output method="html"/>

  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
      <html>
          <head>
              <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
              <title><xsl:value-of select="dc:resource/dc:titles/dc:title"/></title>
              <link
                  rel="stylesheet"
                  href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
                  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
                  crossorigin="anonymous" />
          </head>
          <body>
                <h1><xsl:value-of select="dc:resource/dc:titles/dc:title"/></h1>
                <table>
                    <xsl:apply-templates/>
                </table>
          </body>
      </html>
  </xsl:template>
  <xsl:template match="//dc:*[not(*)]"> 
              <tr>
                  <th><xsl:value-of select="name(.)"/></th>
                  <td><xsl:value-of select="text()"/></td>
              </tr> 
  </xsl:template>

                  


</xsl:stylesheet>
